# rust-hello-world

This project is based on the following article https://itsfoss.com/rust-introduction/

## Run

```shell
$ rustc main.rs --out-dir target
$ ./target/main
Hello world!
```
